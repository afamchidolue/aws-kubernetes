## install kubectl in jenkins cointainer
ssh root@<ip-address>
docker ps
docker exec -u 0 -it <container_id> bash (root user)
install kubectl for linux following the steps on kubectl
## Install AWS cli
install cli in jenkins container
## Create kubeconfig file for jenkins
in git gitbash  paste "aws eks update-kubeconfig --name < cluster_name>"
cat .kube/config
copy the file
in the jenkins container,there is no editor, so we creat the editor in the server and copy it to the docker container
create a config in the sever that is hosting jenkins,
vim config and paste the config file from number 10
docker exec -it <container_id> bash (non root user)
mkdir .kube
exit
docker cp config <container_id>:/var/jenkins_home/.kube/
## Add AWS credentials to jenkins
in the multi-branch pipeline , create the global credentials
select secret text as kind and create the jenkins_aws_secret_access_key
select secret text as kind and create the jenkins_aws_access_key_id
write the script for the Jenkinsfile
push the changes to git
kubectl get pod

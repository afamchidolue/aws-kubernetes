## create ECR RePository
create the private ECR repository java-maven-app
# create a jenkins Credentials for AWS ECR
go the global credentials in jenkins select usernamewithpassword
add username AWS
add password < aws ecr get-login-password >
ID ecr-credentials
create a jenkins Credentials for ECR 
# create a secret for AWS ECR
kubectl create secret docker-registry aws-registry-key --docker-server=<ECR_URL> --docker-username=AWS --docker-password="aws ecr get-login-password"
## Update the Jenkinsfile
push to gitlab 
build in Jenkins
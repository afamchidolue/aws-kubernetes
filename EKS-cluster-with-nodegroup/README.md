# Project-AWS EKS cluster with a Node Group

# create a EKS IAM role
click on create a role
Click on AWS servives
assign the role to EKS-cluster use-case
create the role using the name eks-cluster-role
# create VPC for the cluster using cloudformation
go to cloudformation 
in the amazon s3 URL, paste the url to the template https://s3.us-west-2.amazonaws.com/amazon-eks/cloudformation/2020-10-29/amazon-eks-vpc-private-subnets.yaml
click next and create the stack
# create the EKS cluster
go to the EKS and create the cluster name "eks-cluster-test"
choose the K8S version
select the cluster role
enable envelope encryption Of Kubernetes secrets using KMS
select the vpc created 
select a security group the vpc created 
enable a public and private cluster endpoint
create a cluster
connect to the EKS CLUSTER LOCALLY WITH KUBECTL.
aws configure list. make sure that the region you created the cluster in aws and the region in your local config are the same , if not you have to reconfigure the local region to match the aws region
aws eks update-kubeconfig --name eks-cluster-test
kubectl cluster-info
# create a role for Node Group for worker nodes
create a role for EC2 .click next
assign amazonEKSworkernodePolicy,amazonEC2containerregistryreadonly & AmazonEKS_CNI_policy to the created role
create a role name eks-node-group-role
# create a Node-group
in the created eks cluster , go to the compute section and Add Node-Group 
CREATE A ROLE NAME
choose the node group role created above
configure your instance
configure the Node-group scaling
choose the nodegroup subnets
select ssh key pair and create the node-group
# configure autoscaling 
create a new policy and copy and paste the json file  attached in the folder
attach the policy to the IAM Role created above
go to gitbash  and execute the command "kubectl apply -f https://raw.githubusercontent.com/kubernetes/autoscaler/master/cluster-autoscaler/cloudprovider/aws/examples/cluster-autoscaler-autodiscover.yaml"
kubectl edit deployment  -n kube-system Cluster-autoscaler .edit the cluster to include some annotation, eks cluster name and version
kubectl get pods -n kube-sytem
kubectl get nodes
kubectl apply -f nginx-config.yaml
you can change your replica to 20 in the nginx-config.yaml file and see

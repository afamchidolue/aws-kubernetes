## create a LKE cluster in Linode 
Download the kubeconfig file
export KUBECONFIG=~/Downloads/test-kubeconfig.yaml
kubectl get node
## Add LKE credentials on Jenkins
in the multi branch pipeline, create a credentials
select secret file as the kind odf secret
select the test-kubeconfig.yaml to upload and in the ID, type Lke-credentials
## install Kubenetes plugin on Jenkins
on manage jenkins go to plugin manager
download Kubernetes cli plugin and install
# Configure Jenkinsfile to deloy to LKE cluster
Build